""" turtle setup script for python wheel / egg

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
from setuptools import setup, find_packages
import turtlevcs.package_information as info

dist = setup(
    name=info.PYTHON_PACKAGE_NAME,
    version=info.VERSION,
    description=info.DESCRIPTION,
    author=info.AUTHOR,
    author_email=info.AUTHOR_EMAIL,
    license=info.LICENSE,
    url=info.URL,
    install_requires=
    [
        "pygit2>=1.14.1",
        "dbus-python>=1.3.2",
        "secretstorage>=3.3.0",
        "pygobject>=3.48.0",
    ],
    packages=find_packages(),
    package_data=
    {
        "turtlevcs":
        [
            "dialogs/ui/*.ui",
        ]
    }
)
