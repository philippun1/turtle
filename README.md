![app icon](data/icons/hicolor/scalable/apps/de.philippun1.turtle.svg)

# Turtle

Manage your git repositories with easy-to-use dialogs in Nautilus.

Currently these dialogs are implemented:
- commit, add, stage, unstage, revert, resolve
- sync (pull+push), pull, push
- checkout, create branch, merge
- init, clone
- remotes, submodules
- log, diff
- settings and about

![dialogs](img/dialogs.png)

There is also the possibility to perform operations from context menu of table entries in the log and commit window:
- pull, checkout, create branch, merge from log entries
- revert, (un)stage from commit entries

Staging hunks directly is also possible, both from the nautilus plugin and the commit window.

Currently only ssh login is possible for remote operations.

## Nautilus plugin
The Nautilus plugin provides emblems for git repositories and menu entries for the context menu. The emblems will use the icons from your selected theme.

![emblems](img/emblems.png)

![emblems overview](img/emblems-overview.png)

There is also a plugin to compare files and folders directly from the nautilus context menu.

## Plugins for other file managers
There are also plugins for Caja, Nemo and Thunar.

## Run from command line
You can also run the turtle dialogs directly from a terminal
```
turtle_cli <module> [path]
```
Available modules would be every python file in the `turtlevcs/dialogs` folder. To start the log you could use
```
turtle_cli log ~/Projects/turtle
```

## Setup

### Flatpak
There is a flatpak on flathub
```
sudo flatpak install flathub de.philippun1.turtle
```
To use the nautilus plugin with flatpak you can install it directly inside the turtle settings dialog.

### Ubuntu
For Ubuntu 24.04 there is a PPA available
```
sudo add-apt-repository ppa:philippun/turtle
sudo apt update
sudo apt install turtle-cli turtle-nautilus
```

### Turtle Installation
You can install all the relevant files onto your system directly with the install.py script
```
python install.py install [--user]
```
To remove it again you can use
```
python install.py uninstall [--user]
```

### Turtle Python Package
You can install the python package directly with
```
pip3 install . [--user]
```

### Development Setup
You can also setup a development environment
```
python install.py install --dev [--user]
```
This will create all the relevant symlinks to develop Turtle directly in your project folder. To remove the symlinks use the `uninstall` parameter.

If you change the nautilus plugin, you have to restart nautilus with
```
nautilus -q
nautilus --no-desktop
```

## System Requirements
The system requirements are only relevant if you install turtle manually.
- gtk4
- libadwaita        >= 1.2
- python3-pygit2    >= 1.14
- python3-dbus
- python3-gi-cairo
- python3-secretstorage
- nautilus-python   >= 4.0

On Ubuntu/Debian you can run
```
sudo apt install python3-pygit2 python3-gi-cairo python3-dbus python3-secretstorage python3-nautilus meld
```

On Fedora you can run
```
sudo dnf install python3-pygit2 python3-dbus python3-secretstorage nautilus-python meld
```

On Arch you can run
```
sudo pacman -S python-pygit2 python-dbus python-secretstorage nautilus-python meld
```
