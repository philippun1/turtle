""" package information

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""

NAME="turtle"
PYTHON_PACKAGE_NAME="turtlevcs"
VERSION="0.12.1"
DESCRIPTION="pygit2 frontend with adwaita style and nautilus plugin"
AUTHOR="Philipp Unger"
AUTHOR_EMAIL="philipp.unger.1988@gmail.com"
LICENSE="GPL3"
URL="https://gitlab.gnome.org/philippun1/turtle"
