""" install script to install directly on current system

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
import os
import site
import shutil
import subprocess
from pathlib import Path
from argparse import ArgumentParser
import turtlevcs.package_information as info

DRY_RUN = False
DEVELOPMENT = False

CAJA = "caja"
NAUTILUS = "nautilus"
NAUTILUS_GTK3 = "nautilus-gtk3"
NEMO = "nemo"
THUNAR = "thunar"

def _copy_or_link(src, dst):
    action_string = "linking" if DEVELOPMENT else "copying"
    if DRY_RUN:
        action_string += "*"
    print(f"{action_string} {src} -> {dst}")
    if not DRY_RUN:
        try:
            os.makedirs(dst.parent, exist_ok=True)
            if DEVELOPMENT:
                os.symlink(src, dst)
            else:
                if os.path.isfile(src):
                    shutil.copy(src, dst)
                else:
                    shutil.copytree(src, dst, dirs_exist_ok=True)
        except Exception as ex:
            print(f"{action_string} failed: {str(ex)}")

def _remove(src):
    print(f"removing {src}")
    if not DRY_RUN:
        try:
            if os.path.islink(src):
                os.unlink(src)
            elif os.path.isfile(src):
                os.remove(src)
            else:
                shutil.rmtree(src)
        except Exception as ex:
            print(f"removing failed: {str(ex)}")

def _detect_plugin(predefined_plugin="auto") -> tuple[str, str, str]:
    plugin = predefined_plugin

    if plugin == "auto":
        try:
            desktop_list = os.environ.get("XDG_CURRENT_DESKTOP").lower().split(":")

            if "gnome" in desktop_list:
                plugin = NAUTILUS
            elif "mate" in desktop_list:
                plugin = CAJA
            elif "cinnamon" in desktop_list or "x-cinnamon" in desktop_list:
                plugin = NEMO
            elif "xfce" in desktop_list:
                plugin = THUNAR
        except Exception as _ex:
            plugin = "XDG_CURRENT_DESKTOP not defined"

    if plugin == NAUTILUS:
        return NAUTILUS, "nautilus-python/extensions", TURTLE_NAUTILUS_PY
    elif plugin == NAUTILUS_GTK3:
        return NAUTILUS_GTK3, "nautilus-python/extensions", TURTLE_NAUTILUS_GTK3_PY
    elif plugin == CAJA:
        return CAJA, "caja-python/extensions", TURTLE_CAJA_PY
    elif plugin == NEMO:
        return NEMO, "nemo-python/extensions", TURTLE_NEMO_PY
    elif plugin == THUNAR:
        return THUNAR, "thunarx-python/extensions", TURTLE_THUNAR_PY

    raise RuntimeError(f"plugin detection error ({plugin})")

FILE_PATH = Path(__file__).parent.resolve()

parser = ArgumentParser(
        prog=info.NAME,
        description=info.DESCRIPTION,
    )
subparsers = parser.add_subparsers(dest="subparser")
parent_parser = ArgumentParser(add_help=False)
parent_parser.add_argument(
    "--dryrun",
    action="store_true",
    help="only simulate the steps in a dryrun")
parent_parser.add_argument(
    "--dev",
    action="store_true",
    help="create symlinks instead of copies, which makes development easier")
parent_parser.add_argument(
    "--flatpak",
    action="store_true",
    help="only copy the flatpak nautilus plugin")
parent_parser.add_argument(
    "--user",
    action="store_true",
    help="use the user context ($HOME/.local/share) wherever possible")
parent_parser.add_argument(
    "--plugin",
    default=NAUTILUS,
    help="select plugin to be installed, auto will detect the desktop environment" \
         " (default: %(default)s)",
    choices=["auto", CAJA, NAUTILUS, NAUTILUS_GTK3, NEMO, THUNAR])
install_subparser = subparsers.add_parser(
    "install",
    parents=[parent_parser],
    help="install the relevant files via copy or symlink")
uninstall_subparser = subparsers.add_parser(
    "uninstall",
    parents=[parent_parser],
    help="remove all the installed files")

arguments = parser.parse_args()

if not arguments.subparser:
    parser.print_help()
    exit()

DRY_RUN = arguments.dryrun
DEVELOPMENT = arguments.dev

# define paths and path elements
data_icons_hicolor = Path("data/icons/hicolor")
SCHEMA = "de.philippun1.turtle.gschema.xml"
DESKTOP = "de.philippun1.turtle.desktop"
SERVICE = "de.philippun1.turtle.service"
SERVICE_LOCAL = "de.philippun1.turtle.local.service"
scalable_apps = Path("scalable/apps")
symbolic_apps = Path("symbolic/apps")
TURTLE_SVG = "de.philippun1.turtle.svg"
TURTLE_SYMBOLIC_SVG = "de.philippun1.turtle-symbolic.svg"
TURTLE_NAUTILUS_PY = "turtle_nautilus.py"
TURTLE_NAUTILUS_FLATPAK_PY = "turtle_nautilus_flatpak.py"
TURTLE_NAUTILUS_GTK3_PY = "turtle_nautilus-gtk3.py"
TURTLE_NAUTILUS_COMPARE_PY = "turtle_nautilus_compare.py"
TURTLE_CAJA_PY = "turtle_caja.py"
TURTLE_NEMO_PY = "turtle_nemo.py"
TURTLE_THUNAR_PY = "turtle_thunar.py"
TURTLEVCS = "turtlevcs"
TURTLE_CLI = "turtle_cli"
TURTLE_SERVICE = "turtle_service"

if not arguments.user:
    # check for immutable filesystems, i.e. OSTree
    is_root = os.getuid() == 0
    can_write_to_usr_bin = os.access("/usr/bin", os.W_OK)
    if is_root and not can_write_to_usr_bin:
        print("Readonly filesystem detected.")
        print("Please run with parameter --user.")
        exit()

# detect desktop environment and set plugin name accordingly
selected_plugin, plugin_sub_path, plugin_file_name = _detect_plugin(arguments.plugin)

# FIXME: These should respect the xdg-spec
if arguments.user or arguments.flatpak:
    usr_bin = Path.home() / ".local/bin"
    usr_icons_hicolor = Path.home() / ".local/share/icons/hicolor"
    usr_share_applications = Path.home() / ".local/share/applications"
    usr_share_completions = Path.home() / ".local/share/bash-completion/completions"
    usr_share_services = Path.home() / ".local/share/dbus-1/services"
    site_packages = Path(site.getusersitepackages())
    plugin_path = Path.home() / ".local/share" / plugin_sub_path
    schemas_path = Path.home() / ".local/share/glib-2.0/schemas"
    service = SERVICE_LOCAL
else:
    usr_bin = Path("/usr/bin")
    usr_icons_hicolor = Path("/usr/share/icons/hicolor")
    usr_share_applications = Path("/usr/share/applications")
    usr_share_completions = Path("/usr/share/bash-completion/completions")
    usr_share_services = Path("/usr/share/dbus-1/services")
    site_packages = Path(site.getsitepackages()[-1])
    plugin_path = Path("/usr/share") / plugin_sub_path
    schemas_path = Path("/usr/share/glib-2.0/schemas")
    service = SERVICE

# install will create symbolic links
if arguments.subparser == "install":
    if arguments.flatpak:
        if selected_plugin == NAUTILUS:
            _copy_or_link(FILE_PATH / "plugins" / TURTLE_NAUTILUS_FLATPAK_PY,
                    plugin_path / TURTLE_NAUTILUS_FLATPAK_PY)
        else:
            print("Currently only nautilus plugin is supported with flatpak")
    else:
        _copy_or_link(FILE_PATH / TURTLEVCS, site_packages / TURTLEVCS)
        _copy_or_link(FILE_PATH / TURTLE_CLI, usr_bin / TURTLE_CLI)
        _copy_or_link(FILE_PATH / TURTLE_SERVICE, usr_bin / TURTLE_SERVICE)
        _copy_or_link(FILE_PATH / "data" / DESKTOP, usr_share_applications / DESKTOP)
        _copy_or_link(FILE_PATH / "plugins" / plugin_file_name, plugin_path / plugin_file_name)
        if selected_plugin == NAUTILUS:
            _copy_or_link(FILE_PATH / "plugins" / TURTLE_NAUTILUS_COMPARE_PY,
                plugin_path / TURTLE_NAUTILUS_COMPARE_PY)

        _copy_or_link(FILE_PATH / "data" / "completions" / TURTLE_CLI,
            usr_share_completions / TURTLE_CLI)

        _copy_or_link(FILE_PATH / "data" / service,
            usr_share_services / SERVICE)

        _copy_or_link(FILE_PATH / data_icons_hicolor / scalable_apps / TURTLE_SVG,
            usr_icons_hicolor / scalable_apps / TURTLE_SVG)
        _copy_or_link(FILE_PATH / data_icons_hicolor / symbolic_apps / TURTLE_SYMBOLIC_SVG,
            usr_icons_hicolor / symbolic_apps / TURTLE_SYMBOLIC_SVG)
        if not DRY_RUN:
            subprocess.run(["gtk-update-icon-cache", usr_icons_hicolor], check=False)

        _copy_or_link(FILE_PATH / "data" / SCHEMA, schemas_path / SCHEMA)
        if not DRY_RUN:
            subprocess.run(["glib-compile-schemas", schemas_path], check=False)

# uninstall will unlink them
elif arguments.subparser == "uninstall":
    _remove(site_packages / TURTLEVCS)
    _remove(usr_bin / TURTLE_CLI)
    _remove(usr_bin / TURTLE_SERVICE)
    _remove(usr_share_applications / DESKTOP)
    _remove(usr_share_completions / TURTLE_CLI)
    _remove(usr_share_services / SERVICE)
    _remove(plugin_path / plugin_file_name)
    _remove(plugin_path / TURTLE_NAUTILUS_FLATPAK_PY)
    if selected_plugin == NAUTILUS:
        _remove(plugin_path / TURTLE_NAUTILUS_COMPARE_PY)
    _remove(usr_icons_hicolor / scalable_apps / TURTLE_SVG)
    _remove(usr_icons_hicolor / symbolic_apps / TURTLE_SYMBOLIC_SVG)
    _remove(schemas_path / SCHEMA)
    if not DRY_RUN:
        subprocess.run(["glib-compile-schemas", schemas_path], check=False)
