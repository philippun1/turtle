""" tests for turtle service """
import sys
import pytest
from pathlib import Path
import gi
from test_base import TestBase
from turtlevcs.service import TurtleServiceConnector

gi.require_version("Gtk", "4.0")
from gi.repository import GLib

MAIN_LOOP = None
KEY = 123

def _reply_handler(
        key,
        _status,
        _show_emblems,
        _show_turtle_emblem,
        _show_status_emblem,
        _enable_everywhere):
    assert key == KEY
    MAIN_LOOP.quit()

def _error_handler(error):
    assert False, f"error: {error}"

class TestTurtleService(TestBase):
    """ test class for turtle service """

    @pytest.mark.skip
    def test_status_for_path_from_service(self):
        """ test service """

        try:
            # start a GLib event loop
            global MAIN_LOOP
            MAIN_LOOP = GLib.MainLoop.new(None, True)

            connector = TurtleServiceConnector()
            connector._status_for_path_from_service(
                KEY, str(Path(__file__).parent.resolve()), True,
                _reply_handler, _error_handler)


            MAIN_LOOP.run()
        except Exception as _ex:
            pass # repo might not be available

if __name__ == "__main__":
    pytest.main(sys.argv)
