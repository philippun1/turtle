""" tests for turtle plugin """
import sys
import time
from threading import Thread
import pytest
import gi
from gi.repository import GLib
from test_base import TestBase
from turtlevcs.turtle_plugin import TurtlePlugin
from turtlevcs.service import start_service

gi.require_version("Gtk", "4.0")
from gi.repository import Gio, GLib

MAIN_LOOP = None

def create_menu_item(name, label, tip="", icon=""):
    """ create_menu_item mock """
    return MenuItemMock(name, label, tip, icon)

def create_menu():
    """ create_menu mock"""
    return MenuMock()

def finished_callback(_provider, _handle, _closure, _result):
    """ finished_callback """
    MAIN_LOOP.quit()

class FileInfoMock():
    """ FileInfoMock """
    path = None
    emblems = []

    def __init__(self, path):
        self.path = Gio.File.new_for_path(path)

    def get_location(self):
        """ get_location """
        return self.path

    def get_file_type(self):
        """ get_file_type """
        return self.path.query_file_type(Gio.FileQueryInfoFlags.NONE, None)

    def get_uri(self):
        """ get_uri """
        return self.path.get_uri()

    def add_emblem(self, emblem):
        """ add_emblem """
        self.emblems.append(emblem)

class MenuMock():
    """ MenuMock """
    items = []

    def __init__(self):
        pass

    def append_item(self, item):
        """ append_item """
        self.items.append(item)

class MenuItemMock():
    """ MenuItemMock """
    name = None
    label = None
    tip = None
    icon = None

    submenu = None

    def __init__(self, name, label, tip, icon):
        self.name = name
        self.label = label
        self.tip = tip
        self.icon = icon

    def set_submenu(self, submenu):
        """ set_submenu """
        self.submenu = submenu

    def connect(self, name, callback, *data):
        """ connect """

@pytest.mark.usefixtures("path")
class TestTurtlePlugin(TestBase):
    """ test class for turtle plugin """

    def test_calculate_emblems(self, path):
        """ test creating emblems i.e. for nautilus """
        plugin = TurtlePlugin(
            create_menu_item,
            create_menu,
            finished_callback,
            is_test=True)

        nautilus_file = FileInfoMock(path)

        global MAIN_LOOP
        MAIN_LOOP = GLib.MainLoop()
        thread = Thread(target=start_service, args=[MAIN_LOOP])
        thread.start()

        start = time.time()

        plugin.update_file_info(0, 1, 2, nautilus_file)
        # wait for plugin to finish
        while MAIN_LOOP.is_running():
            pass

        end = time.time()
        duration = end - start
        print(f"duration: {duration * 1000}ms")

        emblems = nautilus_file.emblems
        print(f"emblems: {emblems}")

        thread.join()

    def test_create_menus(self, path):
        """ test creating menus i.e. for nautilus """
        plugin = TurtlePlugin(
            create_menu_item,
            create_menu,
            finished_callback,
            is_test=True)

        nautilus_file = FileInfoMock(path)

        start = time.time()

        items = plugin.create_menus([nautilus_file], "test_create_menus")

        end = time.time()
        duration = end - start
        print(f"duration: {duration * 1000}ms")

        assert len(items) > 0

        print("menu items:")

        def print_items(items, indentation=""):
            for item in items:
                print(f"{indentation}{item.label}")
                if item.submenu is not None:
                    print_items(item.submenu.items, indentation=indentation+"  ")

        print_items(items)

if __name__ == "__main__":
    pytest.main(sys.argv)
