""" conftest.py used by pytest """
import pytest

def pytest_addoption(parser):
    """ add possible option for pytest """
    parser.addoption(
        "--path",
        default=".",
        help="path: a path"
    )

def pytest_configure(config):
    """ read available options for pytest """
    _path = config.getoption('--path')

@pytest.fixture
def path(request):
    """ read path option """
    p = request.config.option.path
    return p
