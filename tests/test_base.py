""" test base class """

class TestBase:
    """ test base class """

    def setup_method(self, test_method):
        """ setup_method for pytest """
        print(f"setup {test_method}")

    def teardown_method(self, test_method):
        """ teardown_method for pytest """
        print(f"teardown {test_method}")
