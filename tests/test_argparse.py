""" tests for turtle argparse """
import os
import sys
import pytest
from test_base import TestBase
import turtlevcs.dialogs
from turtlevcs import create_parser


class TestTurtleArgparse(TestBase):
    """ test class for turtle argparse """

    def test_arg_doc_coverage(self):
        """ test if every dialog has a argument """
        parser = create_parser("parser pytest", "description pytest")
        ignored_files = [
            "__init__.py",
            "base.py",
            "commit_table.py",
            "create.py",
            "enter_credentials.py",
            "push_table.py",
        ]
        module_path = os.path.dirname(turtlevcs.dialogs.__file__)
        files = os.listdir(module_path)
        for file in files:
            if file not in ignored_files and file.endswith(".py"):
                parser.parse_args([file.rstrip(".py")])

if __name__ == "__main__":
    pytest.main(sys.argv)
