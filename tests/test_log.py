""" tests for turtle log """
import sys
import time
from pathlib import Path
import pytest
from test_base import TestBase
from turtlevcs.turtle import Turtle
from turtlevcs.dialogs.log import LogCalculator

class TestTurtleLog(TestBase):
    """ test class for turtle log """

    @pytest.mark.skip
    def test_log_speed(self):
        """ test the speed of log computation """

        try:
            repo = Path.home() / "Projects" / "gnome-shell"
            turtle = Turtle(repo)

            start = time.time()
            commit_list = turtle.log(True)
            stop = time.time()

            duration = stop - start
            print(f"log duration: {duration}")

            model_list = []

            log = LogCalculator()
            start = time.time()
            log.calculate_graph(commit_list, model_list)
            stop = time.time()

            duration = stop - start
            print(f"graph duration: {duration}")
        except Exception as _ex:
            pass # repo might not be available

if __name__ == "__main__":
    pytest.main(sys.argv)
