""" turtle plugin for caja

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
import gi
from turtlevcs.turtle_plugin import TurtlePlugin
gi.require_version("Gtk", "3.0")
from gi.repository import Caja, GObject


class TurtleMenuProvider(GObject.GObject, Caja.MenuProvider, Caja.InfoProvider):
    """ menu provider class for caja """

    def __init__(self):
        self.turtle = TurtlePlugin(self.__create_menu_item, self.__create_menu)

    def __create_menu_item(self, name, label, tip="", icon=""):
        item = Caja.MenuItem(name=name,
                                   label=label,
                                   tip=tip,
                                   icon=icon)

        return item

    def __create_menu(self):
        return Caja.Menu()

    def get_file_items(self, _window, files):
        """ get_file_items """
        if len(files) > 0:
            items = self.turtle.create_menus(files, "file")

            return items

    def get_background_items(self, _window, file):
        """ get_background_items """
        items = self.turtle.create_menus([file], "background")

        return items

    def update_file_info(self, file):
        """ update_file_info """
        self.turtle.update_file_info(file)
