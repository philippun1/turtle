""" turtle plugin for thunar

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
import gi
from turtlevcs.turtle_plugin import TurtlePlugin
gi.require_version("Gtk", "3.0")
from gi.repository import Thunarx, GObject


class TurtleMenuProvider(GObject.GObject, Thunarx.MenuProvider):
    """ menu provider class for thunar """

    def __init__(self):
        self.turtle = TurtlePlugin(
            self.__create_menu_item,
            self.__create_menu)

    def __create_menu_item(self, name, label, tip="", icon=""):
        item = Thunarx.MenuItem(
            name=name,
            label=label,
            tooltip=tip,
            icon=icon)

        return item

    def __create_menu(self):
        return Thunarx.Menu()

    def get_file_menu_items(self, _window, files):
        """ get_file_menu_items """
        if len(files) > 0:
            items = self.turtle.create_menus(files, "file")

            return items

    def get_folder_menu_items(self, _window, file):
        """ get_folder_menu_items """
        items = self.turtle.create_menus([file], "background")

        return items
