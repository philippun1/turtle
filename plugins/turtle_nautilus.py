""" turtle plugin for nautilus

This file is part of Turtle.

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
import gi
from turtlevcs.turtle_plugin import TurtlePlugin
gi.require_version("Gtk", "4.0")
from gi.repository import Nautilus, GObject


class TurtleMenuProvider(GObject.GObject, Nautilus.MenuProvider, Nautilus.InfoProvider):
    """ menu and info provider class for nautilus """

    def __init__(self):
        self.turtle = TurtlePlugin(
            self.__create_menu_item,
            self.__create_menu,
            Nautilus.info_provider_update_complete_invoke)

    def __create_menu_item(self, name, label, tip="", icon=""):
        item = Nautilus.MenuItem(
            name=name,
            label=label,
            tip=tip,
            icon=icon)

        return item

    def __create_menu(self):
        return Nautilus.Menu()

    def get_file_items(self, files):
        """ get_file_items from Nautilus.MenuProvider """
        if len(files) > 0:
            items = self.turtle.create_menus(files, "file")

            return items

    def get_background_items(self, file):
        """ get_background_items from Nautilus.MenuProvider """
        items = self.turtle.create_menus([file], "background")

        return items

    def update_file_info_full(self, provider, handle, closure, file):
        """ update_file_info_full from Nautilus.InfoProvider """
        self.turtle.update_file_info(provider, handle, closure, file)

        return Nautilus.OperationResult.IN_PROGRESS
